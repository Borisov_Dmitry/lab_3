//������� �.�. 204-311
#include "stm32f10x.h"

volatile uint16_t LedOn = 1000;
volatile uint16_t LedOff = 1000;
volatile uint16_t LedOff1 = 500;
int flagcnt = 0;

int main(void) {
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

	GPIO_InitTypeDef gpio_init_A8;
	gpio_init_A8.GPIO_Pin = GPIO_Pin_8;
	gpio_init_A8.GPIO_Speed = GPIO_Speed_2MHz;
	gpio_init_A8.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &gpio_init_A8);

	GPIO_InitTypeDef gpio_init_B12;
	gpio_init_B12.GPIO_Pin = GPIO_Pin_12;
	gpio_init_B12.GPIO_Speed = GPIO_Speed_2MHz;
	gpio_init_B12.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOB, &gpio_init_B12);

	GPIO_InitTypeDef gpio_init_C15;
	gpio_init_C15.GPIO_Pin = GPIO_Pin_15;
	gpio_init_C15.GPIO_Speed = GPIO_Speed_2MHz;
	gpio_init_C15.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOC, &gpio_init_C15);

	GPIO_InitTypeDef gpio_init_C14;
	gpio_init_C14.GPIO_Pin = GPIO_Pin_14;
	gpio_init_C14.GPIO_Speed = GPIO_Speed_2MHz;
	gpio_init_C14.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOC, &gpio_init_C14);

	GPIO_InitTypeDef gpio_init_C13;
	gpio_init_C13.GPIO_Pin = GPIO_Pin_13;
	gpio_init_C13.GPIO_Speed = GPIO_Speed_2MHz;
	gpio_init_C13.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOC, &gpio_init_C13);

	TIM_TimeBaseInitTypeDef tim3_init;
	tim3_init.TIM_Prescaler = 36000 - 1;
	tim3_init.TIM_CounterMode = TIM_CounterMode_Up;
	tim3_init.TIM_Period = 1000 - 1;
	tim3_init.TIM_ClockDivision = TIM_CKD_DIV1;
	tim3_init.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM3, &tim3_init);

	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM3, ENABLE);

	NVIC_InitTypeDef NVIC_Init_Tim3;
	NVIC_Init_Tim3.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_Init_Tim3.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_Init_Tim3.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init_Tim3.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_Init_Tim3); // ��������� ���������� ������� 3

	for (;;) {

		if (!(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8))) {
			LedOn = 4000;
		}

		if (!(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_14))) {
			LedOn = 2000;
		}

		if (!(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_15))) {
			LedOn = 3000;
		}

		if (!(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12))) {
			if (flagcnt == 0) {
				GPIO_SetBits(GPIOC, GPIO_Pin_13);
				TIM_SetCounter(TIM3, 0);

				flagcnt = 1;
			}
		}

		else {
			if ((flagcnt == 1)) {
				flagcnt = 0;

				LedOff1 = TIM_GetCounter(TIM3);
				if (LedOff1 > 50) {
					LedOff = LedOff1 * 2;
				}
			}
		}
		if ((GPIOC->IDR & GPIO_IDR_IDR15) && (GPIOC->IDR & GPIO_IDR_IDR14) && (GPIOA->IDR & GPIO_IDR_IDR8)) {
			LedOn = 1000;
		}
	}
}

void TIM3_IRQHandler(void) {

	TIM_ClearFlag(TIM3, TIM_IT_Update);

	if (GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_13)) {
		GPIO_ResetBits(GPIOC, GPIO_Pin_13);
		TIM_SetAutoreload(TIM3, LedOn - 1);
	} else {
		GPIO_SetBits(GPIOC, GPIO_Pin_13);
		TIM_SetAutoreload(TIM3, LedOff - 1);
	}
}

